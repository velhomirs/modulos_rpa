from selenium import webdriver


def chrome_browser(site):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--start-maximized")
    chrome_options.binary_location = "C:/Users/55549/Desktop/Python/chrome-win64 (118_0_5993_70)/chrome-win64/chrome.exe"
    chrome_driver_path = "C:/Users/55549/Desktop/Python/chromedriver-win64 (118_0_5993_70)/chromedriver-win64/chromedriver.exe"
    service_options = webdriver.ChromeService(executable_path=chrome_driver_path)
    driver = webdriver.Chrome(options=chrome_options, service=service_options)

    driver.get(site)

    return driver
